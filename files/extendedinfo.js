// I can never decide on things so lets have options
var madisonmaps = [];
var sapporomaps = [];

// same as above with different colors - japan names not complete
var Esri_WorldTopoMap = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
});
var Esri_WorldTopoMap_ = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community'
});
madisonmaps.push(Esri_WorldTopoMap);
sapporomaps.push(Esri_WorldTopoMap_);

// simple; names and colors - japan names not complete
var Esri_WorldStreetMap = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
});
var Esri_WorldStreetMap_ = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
});
madisonmaps.push(Esri_WorldStreetMap);
sapporomaps.push(Esri_WorldStreetMap_);

// good detailed
var OpenStreetMap_DE = L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
var OpenStreetMap_DE_ = L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
madisonmaps.push(OpenStreetMap_DE);
sapporomaps.push(OpenStreetMap_DE_);

// minimal address numbers only
var Hydda_Full = L.tileLayer('https://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: 'Tiles courtesy of <a href="http://openstreetmap.se/" target="_blank">OpenStreetMap Sweden</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
var Hydda_Full_ = L.tileLayer('https://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png', {
	maxZoom: 18,
	attribution: 'Tiles courtesy of <a href="http://openstreetmap.se/" target="_blank">OpenStreetMap Sweden</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
madisonmaps.push(Hydda_Full);
sapporomaps.push(Hydda_Full_);

// great colors but only street names, buildings blank
var Stamen_Terrain = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	subdomains: 'abcd',
	minZoom: 0,
	maxZoom: 18,
	ext: 'png'
});
var Stamen_Terrain_ = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}', {
	attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
	subdomains: 'abcd',
	minZoom: 0,
	maxZoom: 18,
	ext: 'png'
});
madisonmaps.push(Stamen_Terrain);
sapporomaps.push(Stamen_Terrain_);

// satellite
var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
});
var Esri_WorldImagery_ = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
});
madisonmaps.push(Esri_WorldImagery);
sapporomaps.push(Esri_WorldImagery_);



var content = {};
content['chem'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Chemistry Building <br>• CHEM 103</p>';
content['brogden'] =
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Brogden Psychology Building <br>• CS 302</p>';
content['engr'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Engineering Hall <br>• ECE 252 <br>• INTERENGR 110 <br>• MATH 435</p>';
content['wendt'] = 	
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Wendt Commons <br>• ECE 252</p>';
content['vv'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Van Vleck <br>• MATH 222 <br>• MATH 341 <br>• STAT 311</p>';
content['nancy'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Nacy Nicolas Hall <br>• LSCOM 100</p>';
content['noland'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Noland Zoology Building <br>• CS 367</p>';
content['vh'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Van Hise <br>• Anime Club <br>• JAPANESE 123 <br>• JAPANESE 124 <br>• COMPLIT 203 <br>• JAPANESE 104 <br>• JAPANESE 203</p>';
content['cs'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Computer Sciences <br>• EA 253 <br>• CS 559 <br>• CS 576</p>';
content['aghall'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Agriculture Hall <br>• CS 354</p>';
content['sewell'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Sewell Social Sciences <br>• SOC 170 <br>• MATH 475 <br>• JAPANESE 203 <br>• LITTRANS 231</p>';
content['humanities'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Humanities <br>• Anime Club <br>• CS 577 <br>• HIST 104 <br>• ENGL 140</p>';
content['sciencehall'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Science Hall <br>• CS 536</p>';
content['deluca'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>DeLuca Biochemistry Building <br>• CS 537</p>';
content['soils'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Soils Hall <br>• ENGL 140</p>';
content['slichter'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Slichter Dorm <br>• Freshman year</p>';
content['ogg'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Ogg Dorm <br>• Sophomore year</p>';
content['tripp'] = 
	'<button onclick="popuplocker(this, false)">lock popup</button>'+
	'<p>Tripp Dorm <br>• Junior - Fall</p>';
content['scs'] = 
	'<button onclick="popuplocker(this, true)">lock popup</button>'+
	'<p>Student Communication Station <br>• Japanese Politics <br>• Japanese School and Education</p>';
content['high'] = 
	'<button onclick="popuplocker(this, true)">lock popup</button>'+
	'<p>北海道大学 高等教育推進機構 <br>• Anthropology <br>• How We Learn Languages <br>• Calculus <br>• Linear Algebra <br>• Physics</p>';
content['engrjpn'] = 
	'<button onclick="popuplocker(this, true)">lock popup</button>'+
	'<p>Engineering Hall <br>• Robotics</p>';
content['dormjpn'] = 
	'<button onclick="popuplocker(this, true)">lock popup</button>'+
    '<p>Hokudai International House <br>• Junior - Spring</p>';
    
var contentimages = {};
contentimages['vh'] = 'I like this part of my classroom. It has a good ' +
                        'selection of lecture halls and an even greater display ' +
                        'of table-and-chair rooms. I’ve used both extensively. '+
                        'With that use, I was able to collect some fond memories. ' +
                        'It’s where I learned Japanese and partake in the '+
                        'anime club. I’m still learning Japanese to be precise, '+
                        'just not here. It makes the part of my classroom '+
                        'that’s 5700 miles (your mileage may vary) away seem '+
                        'close. I will say however, it doesn’t make it feel '+
                        'close enough for me to feel like walking there. Walking, '+
                        'by the way, is my usual mode of transportation between '+
                        'parts of my classroom.';
contentimages['chem'] = 'I haven’t used this part of my classroom in years, '+
                        'but this is the only part to go through a major '+
                        'change since my arrival at the University. Currently '+
                        'a Senior (2019), I used this in my Freshman year '+
                        '(2016). Well, I didn’t use <i>this</i> at the time. '+
                        '<i>This</i>, as it happens to be, is a construction '+
                        'site. A <a href="https://news.wisc.edu/construction-begins-on-133-million-revamp-of-chemistry-building/">$133 million construction site </a>'+
                         '(and who says I never splurge). The part I was '+
                         'using happened to be a lecture hall and a lab at '+
                         'the time. I tend to forget the spaces, the tiny '+
                         'nooks and crannies of my classroom after a long '+
                         'period of nonuse. So, it’s nice to be reminded '+
                         'about them every once and a while.';
contentimages['humanities'] = 'I love this place, I do. I love to hate it. '+
                              'I can’t get away from this part of my classroom. '+
                              'I have been and still am disposed to using it. '+
                              'Anime Club has its best meetings here after '+
                              'all, along with some Computer Science, History, '+
                              'and English coursework. I, along with many '+
                              'others, am not a fan of its '+
                              '<a href="https://www.tonemadison.com/articles/towards-an-appreciation-of-brutalism-or-the-humanities-building-is-actually-good">Brutalist style</a>. '+
                              'Not once in my four years of using this part '+
                              'of my classroom have I thought to myself the '+
                              'need for a Humanities 2. The flow of the '+
                              'building is turbulent, especially the first '+
                              'time subjected to it:<br>'+
                              ' • Start with finding and entrance. It doesn’t matter which one, just find one.<br>'+
                              ' • Search the labyrinth for a staircase. Maybe the entrance you found was already a staircase.<br>'+
                              ' • Go to the correct floor as designated by the first digit in the room number.<br>'+
                              ' • Walk down the halls, looking at each room’s number since they aren’t ordered well.<br>'+
                              ' • It’s not here. WHAT! (Ah, you must be looking for 3650. It has its own entrance)';
contentimages['high'] = '日本に留学したことが好きでした。Oops, wrong place. '+
                        'Fortunately, I had the opportunity to extend my '+
                        'classroom across the Pacific and into Japan last '+
                        'Spring. While there, my classroom was mainly in '+
                        'this building. The official name in Japanese is '+
                        '北海道大学 高等教育推進機構 (Hokkaidou daigaku koutou '+
                        'kyouiku suishin kikou). It’s official English name, '+
                        'Institute for the Advancement of Higher  Education, '+
                        'isn’t much better either. This part  of my classroom '+
                        'was the most intimate. Courses would have me share '+
                        'it with only about five to ten other people (though '+
                        'there were plenty of chairs for much more than that). '+
                        'This part of my classroom was just like any other '+
                        'part. A minor exception is that within each set '+
                        'of walls was an independent A/C unit in the ceiling '+
                        'and sink for washing the inevitable chalk residue '+
                        'off your hands.';

var locations = {};
var locationsjpn = {};

locations['chem'] = [43.072476, -89.404595];
locations['brogden'] = [43.072577, -89.406202];
locations['engr'] = [43.071858, -89.41021];
locations['wendt'] = [43.071519, -89.408644];
locations['vv'] = [43.07483, -89.405014];
locations['nancy'] = [43.075731, -89.408771];
locations['noland'] = [43.071583, -89.404473];
locations['vh'] = [43.075502, -89.406753];
locations['cs'] = [43.071512, -89.406696];
locations['aghall'] = [43.075679, -89.41028];
locations['sewell'] = [43.076424, -89.40527];
locations['humanities'] = [43.07419, -89.400124];
locations['sciencehall'] = [43.075882, -89.400986];
locations['deluca'] = [43.073869, -89.410764];
locations['soils'] = [43.076521, -89.411314];
locations['slichter'] = [43.07712, -89.41226];
locations['ogg'] = [43.0706, -89.400017];
locations['tripp'] = [43.078085, -89.41072];

locationsjpn['scs'] = [43.077309, 141.34102];
locationsjpn['high'] = [43.079726, 141.340339];
locationsjpn['engrjpn'] = [43.075753, 141.340753];
locationsjpn['dormjpn'] = [43.071864, 141.35645];


// nice maps but don't really need
// // nice detailed
// var OpenMapSurfer_Roads = L.tileLayer('https://maps.heigit.org/openmapsurfer/tiles/roads/webmercator/{z}/{x}/{y}.png', {
// 	maxZoom: 19,
// 	attribution: 'Imagery from <a href="http://giscience.uni-hd.de/">GIScience Research Group @ University of Heidelberg</a> | Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// });

// // detail, color
// var HikeBike_HikeBike = L.tileLayer('https://tiles.wmflabs.org/hikebike/{z}/{x}/{y}.png', {
// 	maxZoom: 19,
// 	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// });

// // overdetailed
// var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
// 	maxZoom: 19,
// 	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// });