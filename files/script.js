// initialize the map
var madisonmap = L.map('madisonmap').setView([43.07407, -89.40455], 16);
var sapporomap = L.map('sapporomap').setView([43.07605, 141.34523], 15);

madisonmaps[0].addTo(madisonmap);  // load a default map
sapporomaps[0].addTo(sapporomap);  // load a default map

var clearIcon = L.divIcon({className: 'my-div-icon', iconSize: [50, 50]});
var popups = {};
var popupsjpn = {};
var popuplock = false;

// clear things when popups close
madisonmap.on('popupclose', function(e){
	popuplock = false;
	var imgs = document.getElementsByClassName("img");
	for (var i = 0; i < imgs.length; i++) {
		imgs[i].style.display = "none";
	}
	document.getElementById("contenttext").innerHTML = "Please hover your pointer over some of the hotspots to reveal its secrets.";
});
sapporomap.on('popupclose', function(e){
	popuplock = false;
	var imgs = document.getElementsByClassName("img");
	for (var i = 0; i < imgs.length; i++) {
		imgs[i].style.display = "none";
	}
	document.getElementById("contenttext").innerHTML = "Please hover your pointer over some of the hotspots to reveal its secrets.";
});

// only have one map's popup open
madisonmap.on('popupopen', function(e) {
	sapporomap.closePopup();
});
sapporomap.on('popupopen', function(e) {
	madisonmap.closePopup();
});

function setupopups() {
	// setup the popup
	Object.keys(locations).forEach(function(key) {
		popups[key] = L.marker(locations[key]).setIcon(clearIcon).addTo(madisonmap);
		popups[key].on('mouseover', function(e) {
			if(popuplock == false) {
			var a = L.popup()
			.setLatLng(e.latlng) 
			.setContent(content[key])
			.openOn(madisonmap);
			popupcontent(key);
			}
		});
	});

	Object.keys(locationsjpn).forEach(function(key) {
		popupsjpn[key] = L.marker(locationsjpn[key]).setIcon(clearIcon).addTo(sapporomap);
		popupsjpn[key].on('mouseover', function(e) {
			if(popuplock == false) {
			var a = L.popup()
			.setLatLng(e.latlng) 
			.setContent(content[key])
			.openOn(sapporomap);
			popupcontent(key);
			}
		});
	});
}
setupopups();
// display the extended content
function popupcontent(key) {
	if(contentimages[key] === undefined) return;
	document.getElementById(key).style.display = "block";
	document.getElementById(key+"2").style.display = "block";
	document.getElementById("contenttext").innerHTML = contentimages[key];
}

// add ability to "lock" a popup open i.e. disable popup on hover for others
function popuplocker(elem, jpn) {
	if (popuplock) {
		elem.innerHTML = "lock popup";
	} else {
		elem.innerHTML = "unlock"
	}

	if (jpn) {
		madisonmap.closePopup();
	} else {
		sapporomap.closePopup();
	}
	popuplock = !popuplock;
}


// lets do the heatmap
var madisonheat = L.heatLayer([
	// lat, lng, intensity
	[43.072476, -89.404595, 0.164444444],  	// Chem Bld.
	[43.072577, -89.406202, 0.066666667],	// Brogden
	[43.071858, -89.41021, 0.155555556],	// Engr Hall
	[43.071519, -89.408644, 0.044444444],	// Wendt
	[43.07483, -89.405014, 0.222222222],	// Van Vleck
	[43.075731, -89.408771, 0.066666667],	// Nacy Nicolas Hall
	[43.071583, -89.404473, 0.066666667],	// Noland
	[43.075502, -89.406753, 0.924444444],	// Van Hise
	[43.071512, -89.406696, 0.2],			// Computer Sciences
	[43.075679, -89.41028, 0.066666667],	// Ag Hall
	[43.076424, -89.40527, 0.65],			// Sewell Social Science 1/3 real
	[43.07419, -89.400124, 1.0],			// Humanities
	[43.075882, -89.400986, 0.066666667],	// Science Hall
	[43.073869, -89.410764, 0.066666667],	// DeLuca Biochem
	[43.076521, -89.411314, 0.066666667],	// Soils
	// Dorms
	[43.07712, -89.41226, 0.75],	// Slichter
	[43.0706, -89.400017, 0.75],	// Ogg
	[43.078085, -89.41072, 0.65]	// Tripp
], {radius: 40, 
	blur: 25, 
	maxZoom: 1, 
	minOpacity: 0.5,
	gradient: {0: 'navy', 0.35: 'blue', 0.5: 'green', 0.75: 'yellow', 1: 'red'}
	}).addTo(madisonmap);

// lets do the heatmap - japan
var sapporoheat = L.heatLayer([
	// lat, lng, intensity
	[43.077309, 141.34102, 0.3],  	// SCS
	[43.079726, 141.340339, 1.0],  	// High
	[43.075753, 141.340753, 0.1],  	// Engr Bld.
	[43.071864, 141.35645, 0.7],  	// Dorm
], {radius: 40, 
	blur: 25, 
	maxZoom: 1, 
	minOpacity: 0.5,
	gradient: {0: 'navy', 0.35: 'blue', 0.5: 'green', 0.75: 'yellow', 1: 'red'}
	}).addTo(sapporomap);

// change hte map provider
function madisonMapSelect(selection) {
	// remove previous map(s)
	madisonmap.eachLayer(function (layer) {
    	madisonmap.removeLayer(layer);
	});
	// add the slected map
	madisonmaps[selection].addTo(madisonmap);
	madisonheat.addTo(madisonmap);
	setupopups();
}

function sapporoMapSelect(selection) {
	// remove previous map(s)
	sapporomap.eachLayer(function (layer) {
    	sapporomap.removeLayer(layer);
	});
	// add the slected map
	sapporomaps[selection].addTo(sapporomap);
	sapporoheat.addTo(sapporomap);
	setupopups();
}